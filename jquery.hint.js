/**
 * jquery.hint.js jQuery hint plugin 0.1
 * 
 * @author Simon Yang <simonxy@gmail.com>
 * @version 0.1
 * @copyright, no copyright
 * 
 * Examples: $('input').hint(); $('input[title!=""]').hint();
 * $('input').hint({'blurClass' : 'myBlurClass', 'ref' : 'hint'});
 * 
 */

(function($) {

  $.fn.hint = function(options) {
    var opts = $.extend({}, $.fn.hint.defaults, options);

    return this.each(function() {
      // create a reference of the jQuery object
      var self = $(this),

      // initialize hint elements values
      title = self.attr(opts.ref), $form = $(this.form), $win = $(window);

      // remove hint text & style when the input box has focus
      function remove() {
        if (self.val() === title) {
          self.val('');
          if (self.hasClass(opts.blurClass))
            self.removeClass(opts.blurClass);
        }
      }

      // only apply hint effect if the element has hint text set
      // on its tag
      if (title) {
        // apply the hint text & style if there is no user input
        self.blur(function() {
          if (this.value === '') {
            self.val(title).addClass(opts.blurClass);
          }
        }).focus(remove).blur();

        // cleanup hint text on form submission
        $form.submit(remove);
        $win.unload(remove); // Firefox's fix on autocomplete
      }
    });
  };

  $.fn.hint.defaults = {
    blurClass : 'blur',
    ref :   'title'
  }

})(jQuery);
