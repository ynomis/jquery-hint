# Summary #

This is a jQuery plugin to show hint text in html input box. Now some browsers already support this feature natively.

[Code](https://bitbucket.org/ynomis/jquery-hint/src/e020c22f9a0b5a18d352555f8bc350a376970b00/jquery.hint.js?at=master)